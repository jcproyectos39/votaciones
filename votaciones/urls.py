from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from util.views import VotacionListView, votacion, votante, resultado, notificarCorreo, notificarSMS, reenviarCorreosVista

urlpatterns = [
    path('accounts/', include('seguridad.urls'), name='accounts'),
    path('', VotacionListView.as_view(), name='inicio'),
    path('admin/', admin.site.urls),    
    path('util/', include('util.urls')),
    path('resultado_<str:codigo>', resultado, name='resultado'),
    path('votacion/<str:codigo>', votacion, name='votacion'),
    path('<str:codigo>', votante, name='votante'),    
    path('notificar_correo/<str:codigo>', notificarCorreo, name='notificar_correo'),
    path('notificar_sms/<str:codigo>', notificarSMS, name='notificar_sms'),
    path('reenviar_correos/<int:pid>', reenviarCorreosVista, name='reenviar_correos'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = "Votaciones"
