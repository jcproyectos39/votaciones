from twilio.rest import Client
from django.conf import settings
from django.core.mail import send_mail
from util.models import Votante

client = Client(settings.TWILIO_SID, settings.TWILIO_AUTH_TOKEN)

def enviarMensaje(votante, mensaje):
    try:
        message = client.messages.create(
            body=mensaje,
            from_=settings.TWILIO_NUMBER,
            to=settings.TWILIO_PAIS+votante.telefono
        )
    except Exception as e:
        print(e)

def enviarCorreo(votante, mensaje):
    try:        
        send_mail(
            'Sistema de votaciones',
            mensaje,
            settings.DEFAULT_FROM_EMAIL,
            [votante.email],
            fail_silently=False
        )
    except Exception as e:
        print(e)
    
def reenviarCorreos(request, pid):
    votantes = Votante.objects.filter(votacion_id=pid)
    for votante in votantes:
        mensaje = renderizarMensaje(request, votante)
        send_mail(
            'Sistema de votaciones',
            mensaje,
            settings.DEFAULT_FROM_EMAIL,
            [votante.email],
            fail_silently=False
        )
        
    
def renderizarMensaje(request, votante):
    votacion = votante.votacion
    baseurl = request.get_host()
    return 'Votación: '+votacion.nombre+'\n su codigo secreto es '+votante.secreto[-5:]+'\n porfavor siga el enlace para realizar el voto http://'+baseurl+'/'+votante.secreto

