from django.contrib.auth.models import User
from django.db import models
from django.utils.crypto import get_random_string

class Votacion(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100)
    quorum = models.IntegerField()     
    descripcion = models.TextField(null=True, blank=True, default=None) 
    terminado = models.BooleanField(default=False)
    fecha = models.DateTimeField(auto_now=True)
    fecha_apertura = models.DateTimeField()
    fecha_cierre = models.DateTimeField()  
    hash_inicio = models.CharField(max_length=64, null=True, blank=True, default=None)
    hash_fin = models.CharField(max_length=64, null=True, blank=True, default=None)
    TIPO_CHOICES = (
        ('individual', 'PROPUESTAS'),
        ('unico', 'PLANCHA/CANDIDATOS')
    )
    tipo_de_votacion = models.CharField(choices=TIPO_CHOICES, max_length=64, default='unico')
    codigo = models.CharField(max_length=32, default=None, null=True, blank=True)
    
    def __str__(self):
        return self.nombre
    
    def generarCodigo(self):
        Votacion.objects.filter(pk=self.pk).update(codigo=get_random_string(length=32))

class Grupo(models.Model):
    votacion = models.ForeignKey(Votacion, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100)   
    descripcion = models.TextField(null=True, blank=True, default=None)    
    COMO_CHOICES = (
        ('si_no', 'SI / NO'),
        ('a_favor_en_contra', 'A FAVOR / EN CONTRA'),
        ('acuerdo_desacuerdo', 'ACUERDO / DESACUERDO')
    )
    votar_como = models.CharField(choices=COMO_CHOICES, max_length=64, default='si_no', blank=True)
    numero_votos_a_favor = models.IntegerField(default=0, blank=True)
    numero_votos_en_contra = models.IntegerField(default=0, blank=True)
    imagen = models.FileField(upload_to="media/img", null=True, blank=True, default=None)
    
    def __str__(self):
        return self.nombre


class Votante(models.Model):
    votacion = models.ForeignKey(Votacion, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now=True)
    telefono = models.CharField(max_length=100, null=True, blank=True, default=None)
    cedula = models.CharField(max_length=100)
    nombre = models.CharField(max_length=100, null=True, blank=True, default=None)
    email = models.EmailField()
    notificado_sms = models.BooleanField(default=False)    
    notificado_email = models.BooleanField(default=False)
    secreto = models.CharField(max_length=100, null=True, blank=True, default=None)
    terminado = models.BooleanField(default=False)
    
    def __str__(self):
        return self.cedula
    
    def generarCodigo(self):
        Votante.objects.filter(pk=self.pk).update(secreto=get_random_string(length=32))

class Candidato(models.Model):
    grupo = models.ForeignKey(Grupo, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100)
    imagen = models.ImageField(upload_to="media/img", null=True, blank=True, default=None)
    
    def __str__(self):
        return self.nombre
    
class Propuesta(models.Model):
    grupo = models.ForeignKey(Grupo, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100)   
    descripcion = models.TextField(null=True, blank=True, default=None)
    documento = models.FileField(upload_to="documentos/img", null=True, blank=True, default=None)
    
    def __str__(self):
        return self.nombre


class Voto(models.Model):
    votacion = models.ForeignKey(Votacion, on_delete=models.CASCADE)
    votante = models.OneToOneField(Votante, on_delete=models.CASCADE)    
    grupo = models.ForeignKey(Grupo, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now=True)
    a_favor = models.BooleanField(default=False)  
    en_contra = models.BooleanField(default=False)
    secreto = models.CharField(max_length=64)
    
    def __str__(self):
        return self.secreto


