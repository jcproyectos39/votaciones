from django.views.generic import ListView, DetailView 
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.utils import timezone
from django.shortcuts import render, reverse, redirect, get_object_or_404
from django.http import Http404
from .models import *
from .forms import *
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.crypto import get_random_string
import pandas
from django.conf import settings
from .helpers import *


class VotacionDetailView(LoginRequiredMixin, DetailView):
    model = Votacion

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context


class VotacionListView(LoginRequiredMixin, ListView):
    model = Votacion
    paginate_by = 10

    def get_queryset(self):
          return Votacion.objects.filter(usuario__pk=self.request.user.pk)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class VotacionCreate(LoginRequiredMixin, CreateView):
    model = Votacion
    form_class = VotacionForm
    
    def get_success_url(self):
        return reverse('util:votacion_actualizar', kwargs={'pk': self.object.pk})
    
    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.usuario = self.request.user
        return super(VotacionCreate, self).form_valid(form)

class VotacionUpdate(LoginRequiredMixin, UpdateView):
    model = Votacion
    form_class = VotacionForm
    success_url = reverse_lazy('util:votacion_lista')

class VotacionDelete(LoginRequiredMixin, DeleteView):
    model = Votacion
    success_url = reverse_lazy('util:votacion_lista')


class GrupoDetailView(LoginRequiredMixin, DetailView):
    model = Grupo

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context    
    
    def get_success_url(self):
        return reverse('util:grupo_detalle', kwargs={'pk': self.object.pk})


class GrupoListView(LoginRequiredMixin, ListView):
    model = Grupo
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context
    
    def get_queryset(self):
          return Grupo.objects.filter(votacion__usuario=self.request.user)

class GrupoCreate(LoginRequiredMixin, CreateView):
    model = Grupo
    form_class = GrupoForm
    
    def get_success_url(self):
        return reverse('util:grupo_detalle', kwargs={'pk': self.object.pk})

def grupo_votacion_crear(request, pk):
    grupo = Grupo()
    grupo.votacion_id = pk
    grupo.save()
    return redirect('util:grupo_actualizar', pk=grupo.pk)
    
class GrupoUpdate(LoginRequiredMixin, UpdateView):
    model = Grupo
    form_class = GrupoForm
    
    def get_success_url(self):
        return reverse('util:grupo_detalle', kwargs={'pk': self.object.pk})

class GrupoDelete(LoginRequiredMixin, DeleteView):
    model = Grupo    
    
    def get_success_url(self):
        return reverse('util:votacion_actualizar', kwargs={'pk': self.object.votacion.pk})

class VotanteDetailView(LoginRequiredMixin, DetailView):
    model = Votante

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context


class VotanteListView(LoginRequiredMixin, ListView):
    model = Votante
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class VotanteCreate(LoginRequiredMixin, CreateView):
    model = Votante
    form_class = VotanteForm
    
    def get_success_url(self):
        return reverse('util:votacion_actualizar', kwargs={'pk': self.object.votacion.pk})
    
    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.usuario = self.request.user
        return super(VotanteCreate, self).form_valid(form)

class VotanteUpdate(LoginRequiredMixin, UpdateView):
    model = Votante
    form_class = VotanteForm
    
    def get_success_url(self):
        return reverse('util:votacion_actualizar', kwargs={'pk': self.object.votacion.pk})

class VotanteDelete(LoginRequiredMixin, DeleteView):
    model = Votante
    
    def get_success_url(self):
        return reverse('util:votacion_actualizar', kwargs={'pk': self.object.votacion.pk})

class CandidatoDetailView(LoginRequiredMixin, DetailView):
    model = Candidato

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class CandidatoListView(LoginRequiredMixin, ListView):
    model = Candidato
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class CandidatoCreate(LoginRequiredMixin, CreateView):
    model = Candidato
    form_class = CandidatoForm
    
    def get_success_url(self):
        return reverse('util:grupo_detalle', kwargs={'pk': self.object.grupo.pk})
    
    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.usuario = self.request.user
        return super(CandidatoCreate, self).form_valid(form)

class CandidatoUpdate(LoginRequiredMixin, UpdateView):
    model = Candidato
    form_class = CandidatoForm
    
    def get_success_url(self):
        return reverse('util:grupo_detalle', kwargs={'pk': self.object.grupo.pk})

class CandidatoDelete(LoginRequiredMixin, DeleteView):
    model = Candidato
    
    def get_success_url(self):
        return reverse('util:grupo_detalle', kwargs={'pk': self.object.grupo.pk})


class PropuestaDetailView(LoginRequiredMixin, DetailView):
    model = Propuesta

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class PropuestaListView(LoginRequiredMixin, ListView):
    model = Propuesta
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class PropuestaCreate(LoginRequiredMixin, CreateView):
    model = Propuesta
    form_class = PropuestaForm
    
    def get_success_url(self):
        return reverse('util:grupo_detalle', kwargs={'pk': self.object.grupo.pk})
    
    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.usuario = self.request.user
        return super(PropuestaCreate, self).form_valid(form)

class PropuestaUpdate(LoginRequiredMixin, UpdateView):
    model = Propuesta
    form_class = PropuestaForm
    
    def get_success_url(self):
        return reverse('util:grupo_detalle', kwargs={'pk': self.object.grupo.pk})

class PropuestaDelete(LoginRequiredMixin, DeleteView):
    model = Propuesta
    
    def get_success_url(self):
        return reverse('util:grupo_detalle', kwargs={'pk': self.object.grupo.pk})

def notificarCorreo(request, codigo):
    votante = get_object_or_404(Votante, secreto=codigo)
    mensaje = renderizarMensaje(request, votante)
    enviarCorreo(votante, mensaje)
    return redirect('util:votacion_actualizar', pk=votante.votacion.pk)

def notificarSMS(request, codigo):
    votante = get_object_or_404(Votante, secreto=codigo)
    mensaje = renderizarMensaje(request, votante)
    enviarMensaje(votante, mensaje)
    redirect('util:votacion_actualizar', pk=votante.votacion.pk)

def reenviarCorreosVista(request, pid):
    reenviarCorreos(request, pid)
    return redirect('util:votacion_actualizar', pk=pid)

def votante(request, codigo):
    votante = get_object_or_404(Votante, secreto=codigo)
    v = votante.votacion
    mensaje = ''
    if request.method == 'POST':
        if request.POST.get('cedula') == votante.cedula:
            if votante.terminado:            
                return redirect('resultado', codigo=codigo)
            else:
                return redirect('votacion', codigo=codigo)
        else:
            mensaje = 'Votante no valido'
    context = {'votacion': v, 'votante': votante, 'mensaje': mensaje}
    return render(request, 'util/seleccionar_votante.html', context)
    
def votacion(request, codigo):
    votante = get_object_or_404(Votante, secreto=codigo)
    v = votante.votacion
    if not (v.fecha_apertura < timezone.now() < v.fecha_cierre):
        raise Http404
    if request.method == 'POST' and not votante.terminado:
        if v.tipo_de_votacion == 'unico': 
            grupo = get_object_or_404(Grupo, pk=request.POST.get('grupo'))
            voto = Voto.objects.create(
                votante=votante,
                grupo_id=grupo.pk,
                a_favor=True,
                votacion=v
            )
            Grupo.objects.filter(pk=grupo.pk).update(
                        numero_votos_a_favor=(grupo.numero_votos_a_favor+1))
        elif v.tipo_de_votacion == 'individual':
            for grupo in v.grupo_set.all():
                propuesta = request.POST.get('radio'+str(grupo.pk))
                if propuesta == "1":
                    voto = Voto.objects.create(
                        votante=votante,
                        grupo=grupo,
                        votacion=v,
                        a_favor = True
                    ) 
                    Grupo.objects.filter(pk=grupo.pk).update(
                        numero_votos_a_favor=(grupo.numero_votos_a_favor+1))
                elif propuesta == "0":
                    voto = Voto.objects.create(
                        votante=votante,
                        grupo=grupo,
                        votacion=v,
                        en_contra = True
                    )
                    Grupo.objects.filter(pk=grupo.pk).update(
                        numero_votos_en_contra=(grupo.numero_votos_en_contra+1))
        votante.terminado = True
        votante.save()
    if votante.terminado:            
        return redirect('resultado', codigo=codigo)
    context = {'votacion': v, 'votante': votante}
    if v.tipo_de_votacion == 'unico':
        return render(request, 'util/targeton_unico.html', context)
    else:
        return render(request, 'util/targeton_individual.html', context)

def resultado(request, codigo):    
    votante = get_object_or_404(Votante, secreto=codigo)
    v = votante.votacion
    if votante.terminado and votante in v.votante_set.all():
        grupos = v.grupo_set.all().order_by('-numero_votos_a_favor')
        return render(
            request, 'util/resultados_unico.html', {'votante': votante,'votacion': v, 'grupos': grupos})
    raise Http404

def votacionResultado(request, codigo):    
    v = get_object_or_404(Votacion, codigo=codigo)
    grupos = v.grupo_set.all().order_by('-numero_votos_a_favor')
    return render(
        request, 'util/resultados_unico.html', {'votante': None,'votacion': v, 'grupos': grupos})

def importarVotantes(request):
    if request.method == 'POST':
        votacion = get_object_or_404(Votacion, pk=request.POST.get('votacion'))
        documento = request.FILES.get('votantes')
        datap = pandas.read_excel(documento, thousands=',')
        for index, row in datap.iterrows():
            votante, created = Votante.objects.get_or_create(
                votacion_id = votacion.pk,
                cedula = row.get('CEDULA')
            )
            if created:               
                votante.generarCodigo()
                votante = Votante.objects.get(pk=votante.pk)
                mensaje = renderizarMensaje(request, votante)
                #if settings.EMAIL_ENVIAR and not votante.notificado_email:
                    #enviarCorreo(votante, mensaje)
                    #votante.notificado_email = True
                if settings.TWILIO_ENVIAR and not votante.notificado_sms:
                    enviarMensaje(votante, mensaje)
                    votante.notificado_sms = True            
            votante.email = row.get('EMAIL')
            votante.telefono = row.get('TELEFONO')
            votante.nombre = row.get('NOMBRE')
            votante.save()
        return redirect('util:votacion_actualizar', pk=votacion.pk)
    return redirect('inicio')
        
