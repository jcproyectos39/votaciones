from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Votacion

@receiver(post_save, sender=Votacion)
def gen_code(sender, instance, **kwargs):
    instance.generarCodigo()
