from django.apps import AppConfig


class UtilConfig(AppConfig):
    name = 'util'

    def ready(self):
        import util.signals
