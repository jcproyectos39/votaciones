from django.urls import path
from util.views import *


app_name = 'util'

urlpatterns = [
    path('votacion_detalle/<int:pk>', VotacionDetailView.as_view(), name='votacion_detalle'),
    path('votacion_lista', VotacionListView.as_view(), name='votacion_lista'),
    path('votacion_crear', VotacionCreate.as_view(), name='votacion_crear'),
    path('votacion_actualizar/<int:pk>', VotacionUpdate.as_view(), name='votacion_actualizar'),
    path('votacion_borrar<int:pk>/delete/', VotacionDelete.as_view(), name='votacion_borrar'),
    path('votacion_resultado/<str:codigo>', votacionResultado, name='votacion_resultado'),
    path('grupo_detalle/<int:pk>', GrupoDetailView.as_view(), name='grupo_detalle'),
    path('grupo_lista', GrupoListView.as_view(), name='grupo_lista'),
    path('grupo_crear', GrupoCreate.as_view(), name='grupo_crear'),
    path('grupo_votacion_crear/<int:pk>', grupo_votacion_crear, name='grupo_votacion_crear'),
    path('grupo_actualizar/<int:pk>', GrupoUpdate.as_view(), name='grupo_actualizar'),
    path('grupo_borrar<int:pk>/delete/', GrupoDelete.as_view(), name='grupo_borrar'),
    path('candidato_detalle/<int:pk>', CandidatoDetailView.as_view(), name='candidato_detalle'),
    path('candidato_lista', CandidatoListView.as_view(), name='candidato_lista'),
    path('candidato_crear', CandidatoCreate.as_view(), name='candidato_crear'),
    path('candidato_actualizar/<int:pk>', CandidatoUpdate.as_view(), name='candidato_actualizar'),
    path('candidato_borrar<int:pk>/delete/', CandidatoDelete.as_view(), name='candidato_borrar'),    
    path('propuesta_detalle/<int:pk>', PropuestaDetailView.as_view(), name='propuesta_detalle'),
    path('propuesta_lista', PropuestaListView.as_view(), name='propuesta_lista'),
    path('propuesta_crear', PropuestaCreate.as_view(), name='propuesta_crear'),
    path('propuesta_actualizar/<int:pk>', PropuestaUpdate.as_view(), name='propuesta_actualizar'),
    path('propuesta_borrar<int:pk>/delete/', PropuestaDelete.as_view(), name='propuesta_borrar'),
    path('importar_votantes', importarVotantes, name='importar_votantes'),
    path('votante_actualizar/<int:pk>', VotanteUpdate.as_view(), name='votante_actualizar'),
    path('votante_borrar<int:pk>/delete/',VotanteDelete.as_view(), name='votante_borrar'),
]
