# Generated by Django 3.0.6 on 2020-05-15 22:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('util', '0009_auto_20200515_1714'),
    ]

    operations = [
        migrations.AddField(
            model_name='votacion',
            name='url',
            field=models.CharField(blank=True, default=None, max_length=32, null=True),
        ),
    ]
