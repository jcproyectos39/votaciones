# Generated by Django 3.0.7 on 2020-06-10 21:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('util', '0016_auto_20200610_1510'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grupo',
            name='imagen',
            field=models.FileField(blank=True, default=None, null=True, upload_to='media/img'),
        ),
    ]
