from crispy_forms.helper import FormHelper
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm
from crispy_forms.layout import Layout, Row, Column, Submit
from .models import *


class VotacionForm(ModelForm):

    class Meta:
        model = Votacion
        exclude = ['usuario', 'hash_inicio', 'hash_fin', 'terminado']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'upload-form'
        self.helper.layout = Layout(
            
            Row(
                Column('nombre', css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            ),
            Row(
                
                Column('quorum', css_class='form-group col-md-4 mb-0'),
                Column('tipo_de_votacion', css_class='form-group col-md-8 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('fecha_apertura', css_class='form-group col-md-6 mb-0'),
                Column('fecha_cierre', css_class='form-group col-md-6 mb-0'),
                Column('descripcion', css_class='form-group col-md-12 mb-0'),
                css_class='form-row'              
            ),
            Submit('submit', 'Guardar', css_class='float-right')
        )

class VotanteForm(ModelForm):
    class Meta:
        model = Votante
        exclude = ['votacion', 'secreto']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'upload-form'
        self.helper.layout = Layout(
            Row(
                Column('cedula', css_class='form-group col-md-4 mb-0'),
                Column('nombre', css_class='form-group col-md-8 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('email', css_class='form-group col-md-8 mb-0'),
                Column('telefono', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Submit('submit', 'Guardar', css_class='float-right')
        )


class GrupoForm(ModelForm):
    class Meta:
        model = Grupo
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'upload-form'
        layout = Layout(
            Row(
                Column('votacion', css_class='form-group col-md-6 mb-0'),
                Column('nombre', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('descripcion', css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            )
        )
        try:
            if self.instance is not None and self.instance.votacion.tipo_de_votacion == 'individual':
                layout = Layout(
                    layout,
                    Row(
                        Column('votar_como', css_class='form-group col-md-12 mb-0'),
                        css_class='form-row'
                    )
                )
        except Exception as e:
            pass
        self.helper.layout = Layout(
            layout,
            Row(
                Column('imagen', css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            ),
            Submit('submit', 'Continuar', css_class='float-right')
        )


class CandidatoForm(ModelForm):
    class Meta:
        model = Candidato
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'upload-form'
        self.helper.layout = Layout(
            Row(
                Column('grupo', css_class='form-group col-md-4 mb-0'),
                Column('nombre', css_class='form-group col-md-4 mb-0'),
                Column('imagen', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Submit('submit', 'Guardar', css_class='pull-right')
        )

class PropuestaForm(ModelForm):
    class Meta:
        model = Propuesta
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'upload-form'
        self.helper.layout = Layout(
            Row(
                Column('nombre', css_class='form-group col-md-4 mb-0'),
                Column('descripcion', css_class='form-group col-md-8 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('documento', css_class='form-group col-md-8 mb-0'),
                css_class='form-row'
            ),
            Submit('submit', 'Guardar', css_class='pull-right')
        )
