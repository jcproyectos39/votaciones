from django.urls import path

from .views import *

app_name = 'seguridad'

urlpatterns = [
    path('login/', inicioSession, name='inicio_session'),    
    path('logout/', finSession, name='fin_session'),
]
