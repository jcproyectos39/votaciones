from django.contrib.auth.models import User
from django.db import models
from empresa.models import Sucursal


class Usuario(models.Model):
    user = models.OneToOneField(User, related_name='usuario', on_delete=models.CASCADE)
    sucursal = models.ForeignKey(Sucursal, on_delete= models.CASCADE)
