from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render


def inicioSession(request):
    mensaje = ""
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('inicio')
        mensaje = 'usuario o clave incorrectas, porfavor intente de nuevo'
    return render(request, 'seguridad/login.html', {'mensaje': mensaje})

def finSession(request):
    logout(request)
    return render(request, 'seguridad/login.html', {'mensaje': 'su session a finalizado'})