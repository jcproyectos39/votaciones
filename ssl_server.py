#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import webbrowser
from threading import Timer

os.environ["DJANGO_SETTINGS_MODULE"] = "votaciones.settings"

import cherrypy
from django.conf import settings
from django.core.handlers.wsgi import WSGIHandler

import django
django.setup()


class AgapantoApplication(object):
    HOST = "0.0.0.0"
    PORT = 8001
    def mount_static(self, url, root):
        config = {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': root,
            'tools.expires.on': True,
            'tools.expires.secs': 86400
        }
        cherrypy.tree.mount(None, url, {'/': config})

    def run(self):
        cherrypy.config.update({
            'server.socket_host': self.HOST,
            'server.socket_port': int(os.environ.get('PORT', self.PORT)),
            'engine.autoreload_on': False,
            'log.screen': True
        })
        self.mount_static(settings.STATIC_URL, settings.STATIC_ROOT)

        cherrypy.log("Loading and serving Django application")
        cherrypy.tree.graft(WSGIHandler())
        cherrypy.engine.start()      
        cherrypy.engine.block()


if __name__ == "__main__":
    AgapantoApplication().run()
