# Dockerfile


# Pull base image
FROM python


ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


WORKDIR /opsprodex
COPY . /opsprodex/

RUN pip install --upgrade pip
RUN pip install --trusted-host pypi.python.org -r requirements.txt

COPY init.sh /init.sh


EXPOSE 80
CMD ["/init.sh"]
